package test.org.talend.worldcup.business;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.talend.worldcup.business.UnitFactory;
import org.talend.worldcup.business.UserFactory;
import org.talend.worldcup.interfaces.business.IUnitFactory;
import org.talend.worldcup.interfaces.business.IUserFactory;
import org.talend.worldcup.interfaces.view.IRanking;
import org.talend.worldcup.interfaces.view.IRankingFactory;
import org.talend.worldcup.view.RankingFactory;

public class RankingTest {
	private IRankingFactory rankingFactory;
	private IRanking ranking;
	private IUserFactory userFactory;
	private IUnitFactory unitFactory;
	Random rand;
	int randInt;

	@Before
	public void setUp() throws Exception {
		this.rankingFactory = new RankingFactory();
		this.ranking = rankingFactory.getRanking();
		this.userFactory = new UserFactory();
		this.unitFactory = new UnitFactory();
		this.rand = new Random();
		randInt = rand.nextInt(10);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAddUser() {
		for (int i = 0; i < randInt; i++) {
			ranking.addUser(userFactory.getUser("Firstname" + i, "LASTNAME"
					+ i, unitFactory.getUnit("RD")));
		}
		assertTrue(ranking.getRankingList().size() == randInt);
	}

	@Test
	public final void testToString() {
		ranking.addUser(userFactory.getUser("Firstname1", "LASTNAME1", "flastname1", "", 
				unitFactory.getUnit("RD"), 10));
		ranking.addUser(userFactory.getUser("Firstname2", "LASTNAME2", "flastname2", "",
				unitFactory.getUnit("Marketing"), 0));
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("{\"unit\":\"RD\",\"name\":\"Firstname1 LASTNAME1\",\"score\":10},");
		sb.append("{\"unit\":\"Marketing\",\"name\":\"Firstname2 LASTNAME2\",\"score\":0}");
		sb.append("]");
		assertTrue(sb.toString().equals(ranking.toString()));
	}

}
