package org.talend.worldcup.exception;

@SuppressWarnings("serial")
public class NotStartedException extends Exception{

	public NotStartedException(String message) {
		super(message);	
	}

}
