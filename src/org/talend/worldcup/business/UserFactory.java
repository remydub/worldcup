package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.ICompany;
import org.talend.worldcup.interfaces.business.IUnit;
import org.talend.worldcup.interfaces.business.IUser;
import org.talend.worldcup.interfaces.business.IUserFactory;

public class UserFactory implements IUserFactory {
	public IUser getUser() {
		return new User();
	}
	
	public IUser getUser(String userFirstname, String userLastname, IUnit userUnit) {
		return new User(userFirstname, userLastname, userUnit);
	}
	
	public IUser getUser(String userFirstname, String userLastname, IUnit userUnit, int score) {
		return new User(userFirstname, userLastname, userUnit, score);
	}

	public IUser getUser(String userFirstname, String userLastname, String userName, String userPassword, IUnit userUnit, int score) {
		return new User(userFirstname, userName, userPassword, userLastname, userUnit, score);
	}

	public IUser getUser(String userFirstname, String userLastname,
			String userName, String userPassword, IUnit userUnit, int score,
			ICompany userCompany) {
		return new User(userFirstname, userName, userPassword, userLastname, userUnit, score, userCompany);
	}
}
