package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IGroup;
import org.talend.worldcup.interfaces.business.ITeam;

public class Team implements ITeam {
	private int mTeamId;
	private String mTeamName;
	private String mTeamFlag;
	private IGroup mTeamGroup;

	protected Team(int teamId, String teamName, String teamFlag,
			IGroup teamGroup) {
		this.mTeamId = teamId;
		this.mTeamName = teamName;
		this.mTeamFlag = teamFlag;
		this.mTeamGroup = teamGroup;
	}

	public int getTeamId() {
		return this.mTeamId;
	}

	public void setTeamId(int teamId) {
		this.mTeamId = teamId;
	}

	public String getTeamName() {
		return this.mTeamName;
	}

	public void setTeamName(String teamName) {
		this.mTeamName = teamName;
	}

	public String getTeamFlag() {
		return this.mTeamFlag;
	}

	public void setTeamFlag(String teamFlag) {
		this.mTeamFlag = teamFlag;
	}

	public IGroup getTeamGroup() {
		return this.mTeamGroup;
	}

	public void setTeamGroup(IGroup teamGroup) {
		this.mTeamGroup = teamGroup;
	}
}
