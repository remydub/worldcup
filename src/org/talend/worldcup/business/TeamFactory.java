package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IGroup;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.business.ITeamFactory;

public class TeamFactory implements ITeamFactory {
	public ITeam getTeam(int teamId, String teamFlag, String teamName,
			IGroup teamGroup) {
		return new Team(teamId, teamName, teamFlag, teamGroup);
	}
}
