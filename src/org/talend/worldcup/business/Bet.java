package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.business.IGame;

public class Bet implements IBet {
	private IGame mGame;
	private String mUsername;
	
	public Bet(IGame game, String username) {
		this.mGame = game;
		this.mUsername = username;
	}

	public IGame getGame() { return mGame; }
	public void setGame(IGame mGame) { this.mGame = mGame; }
	public String getUsername() { return mUsername; }
	public void setUsername(String mUsername) {	this.mUsername = mUsername;	}
}
