package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IUnit;
import org.talend.worldcup.interfaces.business.IUnitFactory;

public class UnitFactory implements IUnitFactory {
	public IUnit getUnit(String unitName) {
		return new Unit(unitName);
	}
}
