package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.business.IBetFactory;
import org.talend.worldcup.interfaces.business.IGame;

public class BetFactory implements IBetFactory {

	public IBet getBet(IGame game, String username) {
		return new Bet(game, username);
	}

	
}
