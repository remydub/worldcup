package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.ICompany;
import org.talend.worldcup.interfaces.business.ICompanyFactory;

public class CompanyFactory implements ICompanyFactory {

	public ICompany getCompany(String companyName) {
		return new Company(companyName);
	}

}
