package org.talend.worldcup.business;

import java.util.Date;
import java.util.Map;

import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;

public class Game implements IGame {
	private int mGameId;
	private boolean mGamePlayed;
	private Date mGameDate;
	private Map<ITeam, Integer> mTeamOne;

	public Game(int gameId, boolean gamePlayed, Date gameDate,
			Map<ITeam, Integer> teamOne) {
		this.mGameId = gameId;
		this.mGamePlayed = gamePlayed;
		this.mGameDate = gameDate;
		this.mTeamOne = teamOne;
	}

	public int getGameId() {
		return mGameId;
	}

	public void setGameId(int mGameId) {
		this.mGameId = mGameId;
	}

	public boolean isGamePlayed() {
		return mGamePlayed;
	}

	public void setGamePlayed(boolean mGamePlayed) {
		this.mGamePlayed = mGamePlayed;
	}

	public Date getGameDate() {
		return mGameDate;
	}

	public void setGameDate(Date mGameDate) {
		this.mGameDate = mGameDate;
	}

	public Map<ITeam, Integer> getTeamOne() {
		return mTeamOne;
	}

	public void setTeamOne(Map<ITeam, Integer> mTeamOne) {
		this.mTeamOne = mTeamOne;
	}
}
