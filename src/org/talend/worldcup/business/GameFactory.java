package org.talend.worldcup.business;

import java.util.Date;
import org.talend.worldcup.interfaces.business.IGameFactory;
import java.util.Map;

import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;

public class GameFactory implements IGameFactory {
	public IGame getGame(int gameId, boolean gamePlayed, Date gameDate,
			Map<ITeam, Integer> teamOne) {
		return new Game(gameId, gamePlayed, gameDate, teamOne);
	}
}
