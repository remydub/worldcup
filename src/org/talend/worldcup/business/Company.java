package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.ICompany;

public class Company implements ICompany {
	private String mCompanyName;

	protected Company(String companyName) {
		this.mCompanyName = companyName;
	}
	
	/*Getters and Setters*/
	public String getCompanyName() { return this.mCompanyName; }
	public void setCompanyName(String companyName) { this.mCompanyName = companyName; }
}
