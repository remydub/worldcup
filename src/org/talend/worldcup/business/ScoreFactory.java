package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IScore;
import org.talend.worldcup.interfaces.business.IScoreFactory;

public class ScoreFactory implements IScoreFactory {
	public IScore getScore() {
		return new Score();
	}
	
	public IScore getScore(int score) {
		return new Score(score);
	}
}
