package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IUnit;

public class Unit implements IUnit {
	private String mUnitName;

	protected Unit(String unitName) {
		this.mUnitName = unitName;
	}
	
	/*Getters and Setters*/
	public String getUnitName() { return this.mUnitName; }
	public void setUnitName(String unitName) { this.mUnitName = unitName; }
}
