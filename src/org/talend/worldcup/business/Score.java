package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IScore;

public class Score implements IScore {
	private int mScore;
	
	protected Score() {
		this.mScore = 0;
	}
	
	protected Score(int userScore) {
		this.mScore = userScore;
	}
	/*Getters and Setters*/
	public int getScore() { return this.mScore; }
	public void setScore(int score) { this.mScore = score; }
}
