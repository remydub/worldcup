package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IGroup;
import org.talend.worldcup.interfaces.business.IGroupFactory;

public class GroupFactory implements IGroupFactory {
	public IGroup getGroup(char groupId, String groupName) {
		return new Group(groupId, groupName);
	}
}
