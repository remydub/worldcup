package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.IGroup;

public class Group implements IGroup {
	private char mGroupId;
	private String mGroupName;

	protected Group(char groupId, String groupName) {
		this.mGroupId = groupId;
		this.mGroupName = groupName;
	}

	public char getGroupId() {
		return this.mGroupId;
	}

	public void setGroupId(char groupId) {
		this.mGroupId = groupId;
	}

	public String getGroupName() {
		return this.mGroupName;
	}

	public void setGroupName(String groupName) {
		this.mGroupName = groupName;
	}

}
