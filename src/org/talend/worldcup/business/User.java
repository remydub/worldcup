package org.talend.worldcup.business;

import org.talend.worldcup.interfaces.business.ICompany;
import org.talend.worldcup.interfaces.business.IScore;
import org.talend.worldcup.interfaces.business.IUnit;
import org.talend.worldcup.interfaces.business.IUser;

public class User implements IUser{
	private String mUserFirstname;
	private String mUserLastname;
	private String mUserName;
	private String mUserPassword;
	private IUnit mUserUnit;
	private IScore mUserScore;
	private ICompany mUserCompany;
	
	protected User() {
	}
	
	protected User(String userFirstname, String userLastname, IUnit userUnit) {
		this.mUserFirstname = userFirstname;
		this.mUserLastname = userLastname;
		this.mUserUnit = userUnit;
		this.mUserScore = new ScoreFactory().getScore();
	}
	
	protected User(String userFirstname, String userLastname, IUnit userUnit, int userScore) {
		this.mUserFirstname = userFirstname;
		this.mUserLastname = userLastname;
		this.mUserUnit = userUnit;
		this.mUserScore = new ScoreFactory().getScore(userScore);
	}
	
	protected User(String userFirstname, String userName, String userPassword, String userLastname, IUnit userUnit, int userScore) {
		this.mUserFirstname = userFirstname;
		this.mUserLastname = userLastname;
		this.mUserName = userName;
		this.mUserPassword = userPassword;
		this.mUserUnit = userUnit;
		this.mUserScore = new ScoreFactory().getScore(userScore);
	}
	
	protected User(String userFirstname, String userName, String userPassword, String userLastname, IUnit userUnit, int userScore, ICompany userCompany) {
		this.mUserFirstname = userFirstname;
		this.mUserLastname = userLastname;
		this.mUserName = userName;
		this.mUserPassword = userPassword;
		this.mUserUnit = userUnit;
		this.mUserScore = new ScoreFactory().getScore(userScore);
		this.mUserCompany = userCompany;
	}
	
	public String toString() {
		String toReturn = "";
		toReturn="{\"username\":\""+this.getUserName()+"\", \"firstname\":\""+this.getUserFirstname().replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"")+"\", \"lastname\":\""+this.getUserLastname().replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"")+"\"}";
		return toReturn;
		
	}
	
	/*Getters and Setters*/
	public String getUserFirstname() { return this.mUserFirstname; }
	public void setUserFirstname(String userFirstname) { this.mUserFirstname = userFirstname; }
	public String getUserName() { return this.mUserName; }
	public void setUserName(String userName) { this.mUserName = userName; }
	public String getUserPassword() { return this.mUserPassword; }
	public void setUserPassword(String userPassword) { this.mUserPassword = userPassword; }
	public String getUserLastname() { return this.mUserLastname; }
	public void setUserLastname(String userLastname) { this.mUserLastname = userLastname; }
	public IUnit getUserUnit() {	return this.mUserUnit; }
	public void setUserUnit(IUnit userUnit) { this.mUserUnit = userUnit; }
	public IScore getUserScore() {	return this.mUserScore; }
	public void setUserScore(IScore userScore) { this.mUserScore = userScore; }
	public ICompany getUserCompany() { return this.mUserCompany; }
	public void setUserCompany(ICompany userCompany) { this.mUserCompany = userCompany;	}	
}
