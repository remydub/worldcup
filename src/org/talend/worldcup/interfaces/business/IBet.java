package org.talend.worldcup.interfaces.business;

public interface IBet {
	IGame getGame();
	void setGame(IGame mGame);
	String getUsername();
	void setUsername(String mUsername);
}
