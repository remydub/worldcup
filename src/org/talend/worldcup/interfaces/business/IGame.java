package org.talend.worldcup.interfaces.business;

import java.util.Date;
import java.util.Map;

public interface IGame {
	int getGameId();
	void setGameId(int mGameId);
	boolean isGamePlayed();
	void setGamePlayed(boolean mGamePlayed);
	Date getGameDate();
	void setGameDate(Date mGameDate);
	Map<ITeam, Integer> getTeamOne();
	void setTeamOne(Map<ITeam, Integer> mTeamOne);
}
