package org.talend.worldcup.interfaces.business;

public interface IUnit {
	String getUnitName();
	void setUnitName(String unitName);
}
