package org.talend.worldcup.interfaces.business;

public interface IUnitFactory {
	IUnit getUnit(String unitName);
}
