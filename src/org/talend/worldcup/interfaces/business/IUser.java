package org.talend.worldcup.interfaces.business;

public interface IUser {
	String getUserFirstname();
	void setUserFirstname(String userFirstname);
	String getUserName();
	void setUserName(String userName);
	String getUserPassword();
	void setUserPassword(String userPassword);
	String getUserLastname();
	void setUserLastname(String userLastname);
	IUnit getUserUnit();
	void setUserUnit(IUnit userUnit);
	IScore getUserScore();
	void setUserScore(IScore userScore);
	ICompany getUserCompany();
	void setUserCompany(ICompany userCompany);
}
