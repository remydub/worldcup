package org.talend.worldcup.interfaces.business;

public interface ITeam {
	int getTeamId();
	void setTeamId(int teamId);
	String getTeamName();
	void setTeamName(String teamName);
	String getTeamFlag();
	void setTeamFlag(String teamFlag);
	IGroup getTeamGroup();
	void setTeamGroup(IGroup teamGroup);
}
