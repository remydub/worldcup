package org.talend.worldcup.interfaces.business;

public interface IScoreFactory {
	
	IScore getScore();
	IScore getScore(int score);
}
