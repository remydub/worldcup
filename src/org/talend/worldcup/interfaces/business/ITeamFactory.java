package org.talend.worldcup.interfaces.business;

public interface ITeamFactory {
	ITeam getTeam(int teamId, String teamFlag, String teamName, IGroup teamGroup);
}
