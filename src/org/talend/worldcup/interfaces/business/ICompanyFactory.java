package org.talend.worldcup.interfaces.business;

public interface ICompanyFactory {
	ICompany getCompany(String companyName);
}
