package org.talend.worldcup.interfaces.business;

import java.util.Date;
import java.util.Map;

public interface IGameFactory {
	IGame getGame(int gameId, boolean gamePlayed, Date gameDate,
			Map<ITeam, Integer> teamOne);
}
