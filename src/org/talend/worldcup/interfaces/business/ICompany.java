package org.talend.worldcup.interfaces.business;

public interface ICompany {
	String getCompanyName();
	void setCompanyName(String companyName);
}
