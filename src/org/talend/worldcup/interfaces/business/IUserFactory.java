package org.talend.worldcup.interfaces.business;

public interface IUserFactory {
	IUser getUser();
	IUser getUser(String userFirstname, String userLastname,
			IUnit userUnit);
	IUser getUser(String userFirstname, String userLastname,
			IUnit userUnit, int score);
	IUser getUser(String userFirstname, String userLastname, String userName, String userPassword,
			IUnit userUnit, int score);
	IUser getUser(String userFirstname, String userLastname, String userName, String userPassword, IUnit userUnit, int score, ICompany userCompany);

}
