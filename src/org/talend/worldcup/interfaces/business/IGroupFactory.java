package org.talend.worldcup.interfaces.business;

public interface IGroupFactory {
	IGroup getGroup(char groupId, String groupName);
}
