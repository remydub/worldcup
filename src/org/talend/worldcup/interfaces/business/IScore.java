package org.talend.worldcup.interfaces.business;

public interface IScore {
	int getScore();
	void setScore(int score);
}
