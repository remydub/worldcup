package org.talend.worldcup.interfaces.business;

public interface IBetFactory {
	IBet getBet(IGame game, String username);
}