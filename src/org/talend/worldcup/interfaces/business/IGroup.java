package org.talend.worldcup.interfaces.business;

public interface IGroup {
	char getGroupId();
	void setGroupId(char groupId);
	String getGroupName();
	void setGroupName(String groupName);
}
