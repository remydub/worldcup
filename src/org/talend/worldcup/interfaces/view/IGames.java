package org.talend.worldcup.interfaces.view;

import org.json.simple.JSONArray;

public interface IGames {
	String toString();
	JSONArray getJSONArray();
}
