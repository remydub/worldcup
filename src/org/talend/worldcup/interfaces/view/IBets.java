package org.talend.worldcup.interfaces.view;

import org.json.simple.JSONArray;

public interface IBets {
	String toString();
	JSONArray getJSONArray();
}
