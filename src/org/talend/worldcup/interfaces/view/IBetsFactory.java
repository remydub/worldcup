package org.talend.worldcup.interfaces.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IBet;

public interface IBetsFactory {
	IBets getBets();
	IBets getBets(List<IBet> betList);
}
