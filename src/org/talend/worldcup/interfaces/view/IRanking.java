package org.talend.worldcup.interfaces.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IUser;

public interface IRanking {
	List<IUser> getRankingList();
	void setRankingList(List<IUser> mRankingList);
	void addUser(IUser user);
	
	String toString();
}
