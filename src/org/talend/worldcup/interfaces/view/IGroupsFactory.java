package org.talend.worldcup.interfaces.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;

public interface IGroupsFactory {
	IGroups getGroups();
	IGroups getGroups(List<ITeam> teamList, List<IGame> gameList);
	IGroups getGroups(List<ITeam> teamList);
}
