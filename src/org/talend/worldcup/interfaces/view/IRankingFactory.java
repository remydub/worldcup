package org.talend.worldcup.interfaces.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IUser;

public interface IRankingFactory {
	
	IRanking getRanking();
	IRanking getRanking(List<IUser> userList);
}
