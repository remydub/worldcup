package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.talend.worldcup.business.UserFactory;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.interfaces.business.IUser;
import org.talend.worldcup.interfaces.business.IUserFactory;

public class UserData {

	private static Logger log = Logger.getLogger(UserData.class);

	private Connection mConnection;

	public UserData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	public boolean registerUser(String username, String password,
			String firstname, String lastname, String companyname,
			String companypassword, String department) throws Exception {

		ResultSet result = QueryUtil.executeQuery(mConnection,
				"select companyid, companypassword from companies where companyname='"
						+ companyname + "'");

		int companyid = -1;
		String companypasswd = "";
		while (result.next()) {
			companyid = result.getInt("companyid");
			companypasswd = result.getString("companypassword");
		}
		result.close();

		if (companypassword == null || !(companypassword.equals(companypasswd))) {
			throw new Exception(
					"The password of the selected company is wrong. Please try again.");
		}

		if (companyid < 0) {
			throw new Exception(
					"The company you have selected is not found in the database. Please contact the administrator.");
		}

		int i = 0;
		PreparedStatement stmt = mConnection
				.prepareStatement("insert into users (username, password, firstname, lastname, score, usercompany) values (?,?,?,?,?,?)");

		try {
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString(3, firstname);
			stmt.setString(4, lastname);
			stmt.setInt(5, 0);
			stmt.setInt(6, companyid);
			i = stmt.executeUpdate();
		} catch (Exception e) {
			throw new Exception("The username is already used.");
		} finally {
			if (stmt != null)
				stmt.close();
		}

		return i >= 1;
	}

	public boolean login(String username, String password) throws Exception {
		PreparedStatement stmt = mConnection
				.prepareStatement("select password from users where username=?");
		ResultSet result = null;
		try {
			stmt.setString(1, username);
			result = stmt.executeQuery();

			String passwd = "";
			while (result != null && result.next()) {
				passwd = result.getString("password");
			}
			if (result != null) {
				result.close();
			}
			if (passwd != null && passwd.equals(password)) {
				log.debug("Password correct for " + username);
				return true;
			} else {
				log.debug("Password incorrect for " + username);
				return false;
			}

		} catch (SQLException e) {
			throw e;
		} finally {
			if (stmt != null)
				stmt.close();
		}
	}

	public boolean updateUser(String username, String firstname, String lastname)
			throws SQLException {
		PreparedStatement stmt = mConnection
				.prepareStatement("UPDATE users set firstname=?, lastname=? where username=?");
		try {
			stmt.setString(1, firstname);
			stmt.setString(2, lastname);
			stmt.setString(3, username);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw e;
		} finally {
			if (stmt != null)
				stmt.close();
		}

		return true;
	}

	public String getRandomAsciiString(String username) throws Exception {
		String randomString = RandomStringUtils.randomAlphanumeric(15);
		int i = 0;
		try {
			i = QueryUtil.execute(mConnection, "UPDATE users set ascii='"
					+ randomString + "' where username='" + username + "'");

		} catch (Exception e) {
			throw e;
		}

		if (i <= 0) {
			throw new Exception("The user " + username + " doesn't exist.");
		}
		return randomString;
	}

	public void updatePassword(String username, String password, String id)
			throws Exception {
		ResultSet result = null;
		try {
			result = QueryUtil
					.executeQuery(mConnection,
							"select ascii from users where username='"
									+ username + "'");
		} catch (Exception e) {
			throw e;
		}

		String retrievedId = "";
		while (result != null && result.next()) {
			retrievedId = result.getString("ascii");
		}

		if (retrievedId != null && retrievedId.equals(id)) {
			PreparedStatement stmt = mConnection
					.prepareStatement("UPDATE users set password=?, ascii=NULL where username=?");
			try {
				stmt.setString(1, password);
				stmt.setString(2, username);
				stmt.executeUpdate();
			} catch (SQLException e) {
				throw e;
			} finally {
				if (stmt != null)
					stmt.close();
			}
		} else {
			throw new SQLException(
					"Unable to update the password. Please try again. Be sure to copy/paste the URL sent by email.");
		}
	}

	public IUser getUser(String email) throws Exception {
		ResultSet result = null;
		try {
			result = QueryUtil.executeQuery(mConnection,
					"select username, firstname, lastname from users where username='"
							+ email + "'");

			IUserFactory userFact = new UserFactory();
			IUser user = userFact.getUser();
			while (result.next()) {
				user.setUserName(email);
				user.setUserFirstname(result.getString("firstname"));
				user.setUserLastname(result.getString("lastname"));
			}

			return user;
		} catch (Exception e) {
			throw e;
		}
	}
}
