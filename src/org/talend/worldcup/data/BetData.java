package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.talend.worldcup.business.BetFactory;
import org.talend.worldcup.business.GameFactory;
import org.talend.worldcup.business.GroupFactory;
import org.talend.worldcup.business.TeamFactory;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.business.IBetFactory;
import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.IGameFactory;
import org.talend.worldcup.interfaces.business.IGroupFactory;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.business.ITeamFactory;
import org.talend.worldcup.interfaces.view.IBets;
import org.talend.worldcup.view.BetsFactory;

public class BetData {
	private Connection mConnection;
	
	private static Logger log = Logger.getLogger(BetData.class);

	public BetData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	public IBets getBets(String attribute) throws SQLException {

		ResultSet result = QueryUtil

				.executeQuery(
						mConnection,
						"select gt.gameid as gameid, g.gameplayed as gameplayed, g.gamedate as gamedate, t.teamid as teamid, t.teamflag as teamflag, t.teamname as teamname, t.teamgroup as teamgroup, gr.groupname as groupname, (select score from bets b where b.gameid=g.gameid and b.teamid=t.teamid and b.groupid=t.teamgroup and b.username='"
								+ attribute
								+ "') as score from groups gr,gameteam gt, teams t, games g where gt.groupid=t.teamgroup and gt.teamid=t.teamid and g.gameid=gt.gameid and t.teamgroup=gr.groupid order by gamedate, first desc;");

		IGameFactory gameFact = new GameFactory();
		IBetFactory betFact = new BetFactory();
		ITeamFactory teamFact = new TeamFactory();
		IGroupFactory groupFact = new GroupFactory();

		List<IBet> betList = new ArrayList<IBet>();
		IBet bet = null;
		IGame game = null;
		Map<ITeam, Integer> map = new LinkedHashMap<ITeam, Integer>();

		int previousGameId = 0;
		while (result.next()) {
			int gameId = result.getInt("gameid");
			ITeam team = teamFact.getTeam(result.getInt("teamid"), result
					.getString("teamflag"), result.getString("teamname"),
					groupFact.getGroup(result.getString("teamgroup").charAt(0),
							result.getString("groupname")));

			if (gameId != previousGameId) {
				if (bet != null)
					betList.add(bet);
				map = new LinkedHashMap<ITeam, Integer>();
				int score = (Integer) result.getInt("score");
				map.put(team, result.wasNull() ? null : score);

				game = gameFact.getGame(gameId,
						result.getInt("gameplayed") == 1 ? true : false,
						result.getTimestamp("gamedate"), map);
				bet = betFact.getBet(game, attribute);
			} else {
				int score = (Integer) result.getInt("score");
				map.put(team, result.wasNull() ? null : score);
			}
			previousGameId = gameId;

		}
		if (bet != null)
			betList.add(bet);
		result.close();
		return new BetsFactory().getBets(betList);

	}

	public boolean updateBets(List<IBet> betList) throws SQLException {

		PreparedStatement insertStatement = mConnection
				.prepareStatement("INSERT INTO bets VALUES (?,?,?,?,?)");
		PreparedStatement updateStatement = mConnection
				.prepareStatement("UPDATE bets SET score=? WHERE gameid=? and teamid=? and username=? and groupid=?");

		for (IBet bet : betList) {
			IGame game = bet.getGame();
			Date gameDate = game.getGameDate();
			Date currentDate = new Date(new java.util.Date().getTime());
			String username = bet.getUsername();

			// jump to the next game whether the game already has started.
			if (gameDate.before(currentDate)) {
				if(!game.isGamePlayed())
					log.debug("The user <" + username + "> has tried to update the game <" + game.getGameId() + "> while the game was started already.");
				continue;
			}

			for (Entry<ITeam, Integer> entry : game.getTeamOne().entrySet()) {
				username = bet.getUsername();
				int gameId = game.getGameId();

				int teamId = ((ITeam) entry.getKey()).getTeamId();
				Integer score = entry.getValue() != null ? (Integer) entry
						.getValue() : null;
				int i = 0;

				if (score != null)
					updateStatement.setInt(1, score);
				else
					updateStatement.setNull(1, java.sql.Types.NULL);
				updateStatement.setInt(2, gameId);
				updateStatement.setInt(3, teamId);
				updateStatement.setString(4, username);
				updateStatement.setString(5, new Character(((ITeam) entry.getKey()).getTeamGroup().getGroupId()).toString());
				try {
					i = updateStatement.executeUpdate();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (i == 0) {
					insertStatement.setString(1, username);
					insertStatement.setInt(2, gameId);
					insertStatement.setInt(3, teamId);
					insertStatement.setString(4, new Character(((ITeam) entry.getKey()).getTeamGroup().getGroupId()).toString());
					if (score != null)
						insertStatement.setInt(5, score);
					else
						insertStatement.setNull(5, java.sql.Types.NULL);
					try {
						insertStatement.executeUpdate();
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			}
		}

		insertStatement.close();
		updateStatement.close();
		return true;

	}

	/**
	 * @return
	 * @throws Exception
	 */
	public boolean computeScore() throws Exception {

		Map<String, Integer> scores = new HashMap<String, Integer>();

		// TODO: retrieve played games.
		ResultSet result = QueryUtil
				.executeQuery(
						mConnection,
						"select g.gameid as gameid, teamid, score, first, groupid from gameteam gt, games g where gt.gameid=g.gameid and g.gameplayed=1 order by gameid, first");

		IGameFactory gameFact = new GameFactory();
		ITeamFactory teamFact = new TeamFactory();
		IGroupFactory groupFact = new GroupFactory();
		Map<String, ITeam> teamMap = new LinkedHashMap<String, ITeam>();
		Map<ITeam, Integer> scoreMap = new LinkedHashMap<ITeam, Integer>();
		List<IGame> playedGamesList = new ArrayList<IGame>();
		int gameid = -1;
		int previousGameId = -1;
		while (result.next()) {
			gameid = result.getInt("gameid");
			int teamid = result.getInt("teamid");
			char groupid = (result.getString("groupid")).charAt(0);
			int score = result.getInt("score");

			ITeam team = (ITeam) teamMap.get(teamid+""+groupid);
			if (team == null) {
				team = teamFact.getTeam(teamid, null, null, groupFact.getGroup(groupid, ""));
				teamMap.put(teamid+""+groupid, team);
			}

			if (gameid != previousGameId) {
				scoreMap = new LinkedHashMap<ITeam, Integer>();
			}
			scoreMap.put(team, score);

			if (gameid == previousGameId) {
				playedGamesList.add(gameFact.getGame(gameid, true, null,
						scoreMap));
			}
			previousGameId = gameid;
		}
				
		result.close();

		// TODO: retrieve bets for those games.

		StringBuilder inSubQuery = new StringBuilder();
		boolean isFirst = true;
		for (IGame playedGame : playedGamesList) {
			if (!isFirst) {
				inSubQuery.append(", ");
			} else {
				isFirst = false;
			}
			inSubQuery.append(playedGame.getGameId());
		}

		result = QueryUtil
				.executeQuery(
						mConnection,
						"select bets.username as username, bets.gameid as gameid, bets.teamid as teamid, first, bets.score as score, bets.groupid as groupid from bets, users, teams, gameteam, games, groups where users.username=bets.username and teams.teamid=bets.teamid and teams.teamid=gameteam.teamid and games.gameid=gameteam.gameid and games.gameid=bets.gameid and teams.teamgroup=groups.groupid and teams.teamgroup=gameteam.groupid and bets.groupid=teams.teamgroup and bets.gameid in ("
								+ inSubQuery
								+ ") order by bets.gameid, users.username, first;");

		gameFact = new GameFactory();
		teamFact = new TeamFactory();
		groupFact = new GroupFactory();
		IBetFactory betFact = new BetFactory();
		scoreMap = new LinkedHashMap<ITeam, Integer>();
		List<IBet> betsList = new ArrayList<IBet>();
		gameid = -1;
		previousGameId = -1;
		String previousUsername = "";
		while (result.next()) {
			gameid = result.getInt("gameid");
			int teamid = result.getInt("teamid");
			char groupid = (result.getString("groupid")).charAt(0);
			Integer score = result.getInt("score");
			if (result.wasNull())
				score = null;
			String username = result.getString("username");

			ITeam team = (ITeam) teamMap.get(teamid+""+groupid);
			if (team == null) {
				team = teamFact.getTeam(teamid, null, null, groupFact.getGroup(groupid, ""));
				teamMap.put(teamid+""+groupid, team);
			}

			if (!username.equals(previousUsername)) {
				scoreMap = new LinkedHashMap<ITeam, Integer>();
			}
			scoreMap.put(team, score);

			if (gameid == previousGameId && username.equals(previousUsername)) {
				betsList.add(betFact.getBet(
						gameFact.getGame(gameid, true, null, scoreMap),
						username));
			}
			previousGameId = gameid;
			previousUsername = username;
		}
		
		result.close();

		// TODO: compute score for each player.
		for (IGame g : playedGamesList) {
			int currentGameId = g.getGameId();
			Map<ITeam, Integer> currentMapTeam = g.getTeamOne();
			java.util.Iterator<ITeam> iter = currentMapTeam.keySet().iterator();
			List<ITeam> currentGameTeamList = new ArrayList<ITeam>();
			while (iter.hasNext()) {
				currentGameTeamList.add((ITeam) iter.next());
			}
			for (IBet b : betsList) {
				IGame currentGame = b.getGame();
				if (currentGame.getGameId() == currentGameId) {
					// 5 points
					Integer score1 = currentMapTeam.get(currentGameTeamList
							.get(0));
					Integer score2 = currentMapTeam.get(currentGameTeamList
							.get(1));
					Integer bet1 = currentGame.getTeamOne().get(
							currentGameTeamList.get(0));
					Integer bet2 = currentGame.getTeamOne().get(
							currentGameTeamList.get(1));
					
					if (bet1 != null && bet2 != null) {
						if (score1 == bet1 && score2 == bet2) {
							scores.put(
									b.getUsername(),
									scores.containsKey(b.getUsername()) ? scores
											.get(b.getUsername()) + 3 : 3);
						} else {
							if ((score1 < score2 && bet1 < bet2)
									|| (score1 > score2 && bet1 > bet2)
									|| (score1 == score2 && bet1 == bet2)) {
								scores.put(
										b.getUsername(),
										scores.containsKey(b.getUsername()) ? scores
												.get(b.getUsername()) + 2 : 2);
							}
						}
					}
				}
			}
		}

		// TODO: insert scores.

		PreparedStatement updateStatement = mConnection
				.prepareStatement("UPDATE users SET score=? WHERE username=?");
		for (Entry<String, Integer> e : scores.entrySet()) {
			updateStatement.setInt(1, (Integer) e.getValue());
			updateStatement.setString(2, (String) e.getKey());
			try {
				updateStatement.executeUpdate();
			} catch (Exception ee) {
				throw ee;
			}
		}
		updateStatement.close();

		// returns the computation status.
		return true;

	}

	public static void main(String[] args) throws Exception {
		BetData data = new BetData();
		data.computeScore();
	}

}
