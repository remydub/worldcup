package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.exception.NotStartedException;

public class DetailData {
	private Connection mConnection;

	public DetailData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	@SuppressWarnings("unchecked")
	public String getDetails(int gameid) throws Exception {
		ResultSet result = QueryUtil
				.executeQuery(
						mConnection,
						"select teamname from games, gameteam, teams where games.gameid="
								+ gameid
								+ " and games.gameid=gameteam.gameid and gameteam.teamid=teams.teamid and teamgroup=groupid order by first desc");

		String team1 = "";
		String team2 = "";
		while (result.next()) {
			if ("".equals(team1)) {
				team1 = result.getString("teamname");
			} else {
				team2 = result.getString("teamname");
			}
		}

		result.close();

		result = QueryUtil
				.executeQuery(
						mConnection,
						"select bets.score as score, bets.username as username, bets.gameid as gameid, games.gamedate as gamedate, firstname, lastname, bets.teamid as teamid, first from bets, users, teams, gameteam, games, groups where users.username=bets.username and teams.teamid=bets.teamid and teams.teamid=gameteam.teamid and games.gameid=gameteam.gameid and games.gameid=bets.gameid and teams.teamgroup=bets.groupid and teams.teamgroup=groups.groupid and teams.teamgroup=gameteam.groupid and bets.gameid="
								+ gameid
								+ " order by users.username, first desc");

		JSONArray jsonArray = new JSONArray();
		JSONObject detailObject = new JSONObject();
		JSONObject gameObject = new JSONObject();
		gameObject.put("game", team1 + " - " + team2);
		Integer score1 = 0;
		Integer score2 = 0;
		String previousUsername = "";
		while (result.next()) {
			Date gameDate = result.getTimestamp("gamedate");
			Date currentDate = new Date(new java.util.Date().getTime());
			if (gameDate.after(currentDate))
				throw new NotStartedException("The game has not yet started.");
			String username = result.getString("username");
			Integer score = result.getInt("score");
			if (result.wasNull())
				score = null;
			if (username != null && !username.equals(previousUsername)) {
				detailObject = new JSONObject();
				jsonArray.add(detailObject);
				detailObject.put("firstname", result.getString("firstname"));
				detailObject.put("lastname", result.getString("lastname"));
				score1 = score;
			} else if (username != null && username.equals(previousUsername)) {
				score2 = score;
				detailObject.put("score", (score1 == null ? "" : score1)
						+ " - " + (score2 == null ? "" : score2));
			}
			previousUsername = username;
		}
		gameObject.put("detail", jsonArray);

		return gameObject.toJSONString();

	}
}
