package org.talend.worldcup.data.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Handles the configuration stored in the configuration.properties file.
 */
public class Configuration {

    public static Configuration INSTANCE = new Configuration();

    private Properties props;

    private Configuration() {
        super();
        try {
            props = new Properties();
            final String name = "configuration.properties";
            final InputStream systemResourceAsStream = Configuration.class.getClassLoader().getResourceAsStream(name);
            if (systemResourceAsStream == null) {
                throw new FileNotFoundException("Cannot load file " + name);
            }
            props.load(systemResourceAsStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getAsString(String key) {
        return this.props.getProperty(key);
    }
}
