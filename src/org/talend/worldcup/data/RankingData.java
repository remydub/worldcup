package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.talend.worldcup.business.CompanyFactory;
import org.talend.worldcup.business.UnitFactory;
import org.talend.worldcup.business.UserFactory;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.interfaces.business.ICompanyFactory;
import org.talend.worldcup.interfaces.business.IUnitFactory;
import org.talend.worldcup.interfaces.business.IUser;
import org.talend.worldcup.interfaces.business.IUserFactory;
import org.talend.worldcup.interfaces.view.IRanking;
import org.talend.worldcup.view.RankingFactory;

public class RankingData {

	private Connection mConnection;

	public RankingData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	public IRanking getRanking() throws java.sql.SQLException {
		ResultSet result = QueryUtil
				.executeQuery(
						mConnection,
						"SELECT username, firstname, lastname, score, unitname, companyname FROM companies, users LEFT JOIN units ON (users.userunit=units.unitid) WHERE companies.companyid=users.usercompany ORDER BY score DESC");
		List<IUser> listUsers = new ArrayList<IUser>();
		while (result.next()) {
			IUserFactory userFact = new UserFactory();
			IUnitFactory unitFact = new UnitFactory();
			ICompanyFactory companyFact = new CompanyFactory();
			IUser user = userFact.getUser(result.getString("firstname"),
					result.getString("lastname"), result.getString("username"),
					"", unitFact.getUnit(result.getString("unitname") + ""),
					result.getInt("score"),
					companyFact.getCompany(result.getString("companyname")));
			listUsers.add(user);
		}
		result.close();
		return new RankingFactory().getRanking(listUsers);
	}
}
