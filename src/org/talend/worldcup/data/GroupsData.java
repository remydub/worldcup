package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.talend.worldcup.business.GroupFactory;
import org.talend.worldcup.business.TeamFactory;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.interfaces.business.IGroup;
import org.talend.worldcup.interfaces.business.IGroupFactory;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.business.ITeamFactory;
import org.talend.worldcup.interfaces.view.IGroups;
import org.talend.worldcup.view.GroupsFactory;

public class GroupsData {

	private Connection mConnection;

	public GroupsData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	public IGroups getGroups() throws java.sql.SQLException {
		ResultSet result = QueryUtil
				.executeQuery(
						mConnection,
						"SELECT teamid, teamflag, teamname, groupname, groupid FROM teams t, groups g WHERE t.teamgroup=g.groupid ORDER BY groupname, teamid");
		List<ITeam> teamList = new ArrayList<ITeam>();
		ITeamFactory teamFact = new TeamFactory();
		IGroupFactory groupFact = new GroupFactory();
		char currentGroupId = 0;
		IGroup teamGroup = null;
		while (result.next()) {
			char groupId = result.getString("groupid").charAt(0);
			if (currentGroupId != groupId) {
				currentGroupId = groupId;
				teamGroup = groupFact.getGroup(groupId,
						result.getString("groupname"));
			}

			ITeam team = teamFact.getTeam(result.getInt("teamid"),
					result.getString("teamflag"), result.getString("teamname"),
					teamGroup);
			teamList.add(team);
		}
		result.close();
		return new GroupsFactory().getGroups(teamList);
	}
}
