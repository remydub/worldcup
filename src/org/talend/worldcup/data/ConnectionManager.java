package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;
import org.talend.worldcup.data.common.Configuration;
import org.talend.worldcup.servlets.InitServlet;


public class ConnectionManager {
	
	private static Logger log = Logger.getLogger(ConnectionManager.class);
	
	public static Connection getConnection() throws Exception {
		log.debug("Get a connection from the pool");
		if(InitServlet.ds!=null) {
			return InitServlet.ds.getConnection();
		} else {
			String url = Configuration.INSTANCE.getAsString("database.url");

            String driver = Configuration.INSTANCE.getAsString("database.driver");
            Class.forName(driver).newInstance();

            String userName = Configuration.INSTANCE.getAsString("database.username");
            String password = Configuration.INSTANCE.getAsString("database.password");

            return DriverManager.getConnection(url, userName, password);
		}
	}
}
