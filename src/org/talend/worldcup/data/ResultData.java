package org.talend.worldcup.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.talend.worldcup.business.GameFactory;
import org.talend.worldcup.business.GroupFactory;
import org.talend.worldcup.business.TeamFactory;
import org.talend.worldcup.data.common.QueryUtil;
import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.IGameFactory;
import org.talend.worldcup.interfaces.business.IGroupFactory;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.business.ITeamFactory;
import org.talend.worldcup.interfaces.view.IGames;
import org.talend.worldcup.view.GamesFactory;

public class ResultData {

	private Connection mConnection;

	public ResultData() throws Exception {
		mConnection = ConnectionManager.getConnection();
	}

	public IGames getGames() throws java.sql.SQLException {
		ResultSet result = QueryUtil
				.executeQuery(
						mConnection,
						"select gt.gameid as gameid, g.gameplayed as gameplayed, g.gamedate as gamedate, t.teamid as teamid, t.teamflag as teamflag, t.teamname as teamname, t.teamgroup as teamgroup, gr.groupname as groupname, gt.score as score from groups gr,gameteam gt, teams t, games g where gt.groupid=t.teamgroup and gt.teamid=t.teamid and g.gameid=gt.gameid and t.teamgroup=gr.groupid order by gamedate, first desc;");
		ITeamFactory teamFact = new TeamFactory();
		IGroupFactory groupFact = new GroupFactory();
		IGameFactory gameFact = new GameFactory();
		List<IGame> gameList = new ArrayList<IGame>();
		Map<ITeam, Integer> map = new LinkedHashMap<ITeam, Integer>();
		IGame game = null;
		int previousGameId = 0;
		while (result.next()) {
			int gameId = result.getInt("gameid");
			Integer score = result.getInt("score");
			if (result.wasNull())
				score = null;

			ITeam team = teamFact.getTeam(result.getInt("teamid"), result
					.getString("teamflag"), result.getString("teamname"),
					groupFact.getGroup(result.getString("teamgroup").charAt(0),
							result.getString("groupname")));

			if (gameId != previousGameId) {
				if (game != null)
					gameList.add(game);
				map = new LinkedHashMap<ITeam, Integer>();

				map.put(team, score);

				game = gameFact.getGame(gameId,
						result.getInt("gameplayed") == 1 ? true : false,
						result.getTimestamp("gamedate"), map);

			} else {
				map.put(team, score);
			}
			previousGameId = gameId;

		}
		if (game != null)
			gameList.add(game);
		result.close();
		return new GamesFactory().getGames(gameList);
	}
}
