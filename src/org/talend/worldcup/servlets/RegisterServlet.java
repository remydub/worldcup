package org.talend.worldcup.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.talend.worldcup.data.UserData;

public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		BufferedReader in = request.getReader();
		String i;
		String message = null;
		String username = null;
		String password = null;
		String firstname = null;
		String lastname = null;
		String companyName = null;
		String companyPassword = null;
		String departmentName = null;
		
		boolean successfullRegistration = false;
		String exception="";
		
		while ((i = in.readLine()) != null) {
			message = i;
		}
		
		if (message != null) {
			JSONParser parser = new JSONParser();
			try {
				JSONObject objectToParse = (JSONObject) parser.parse(message);
				username = (String) objectToParse.get("username");
				password = (String) objectToParse.get("password");
				firstname = (String) objectToParse.get("firstname");
				lastname = (String) objectToParse.get("lastname");
				companyName = (String) objectToParse.get("companyname");
				companyPassword = (String) objectToParse.get("companypassword");
				departmentName = (String) objectToParse.get("departmentname");
			} catch (ParseException e) {
				exception = e.getMessage();
			}
			
			try {
				UserData data = new UserData();
				successfullRegistration = data.registerUser(username, password, firstname, lastname, companyName, companyPassword, departmentName);
			} catch (Exception e) {
				exception = e.getMessage();
			}
		}
		
		String jsonToSendBack = "{\"status\":\""+(successfullRegistration?"success":"failure")+"\", \"message\":\""+(successfullRegistration?"Your registration has succeeded.":exception)+"\"}";
		PrintWriter out = response.getWriter();
		out.print(jsonToSendBack);
		out.close();
	}
}
