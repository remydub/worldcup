package org.talend.worldcup.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.talend.worldcup.data.RankingData;

public class RankingServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession(false);
		
		PrintWriter out = response.getWriter();
		if(session==null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your session has expired. Please login.\"}");
			out.close();
		} else {
			RankingData data;
			try {
				data = new RankingData();
				out.print(data.getRanking().toString());
			} catch (Exception e) {
				out.print("{\"status\":\"failure\", \"code\":500, \"message\":\""+e.getMessage()+"\"}");
			}
			out.close();
		}
	}
}
