package org.talend.worldcup.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.talend.worldcup.data.UserData;
import org.talend.worldcup.interfaces.business.IUser;

public class ProfileServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(ProfileServlet.class);

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);

		PrintWriter out = response.getWriter();
		if (session == null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your session has expired. Please login.\"}");
			out.close();
		} else {
			String email = (String) session.getAttribute("username");
			UserData data;
			try {
				data = new UserData();
				IUser user = data.getUser(email);
				out.print(user.toString());
			} catch (Exception e) {
				e.printStackTrace();
				out.print("{\"status\":\"failure\", \"code\":500, \"message\":\""
						+ e.getMessage() + "\"}");
			} finally {
				out.close();
			}
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();

		if (session == null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your session has expired. Please login.\"}");
			out.close();
		} else {
			BufferedReader in = request.getReader();
			String i;
			String firstname = null;
			String lastname = null;

			String email = (String) session.getAttribute("username");

			String message = null;
			while ((i = in.readLine()) != null) {
				message = i;
			}

			if (message != null) {
				JSONParser parser = new JSONParser();
				try {
					JSONObject objectToParse = (JSONObject) parser
							.parse(message);
					firstname = (String) objectToParse.get("firstname");
					lastname = (String) objectToParse.get("lastname");

					log.info("Profile update by <" + email + ">");

				} catch (ParseException e) {
					log.info("Profile update failed for <" + email + ">");
				}

				UserData data;
				try {
					data = new UserData();
					data.updateUser(email, firstname, lastname);
				} catch (Exception e) {
					out.write("{\"status\":\"failure\", \"message\":\"The profile update has failed.\"}");
					out.close();
					e.printStackTrace();
				}

				out.write("{\"status\":\"success\", \"message\":\"The profile update has succeeded.\"}");
				out.close();
			} else {
				out.write("{\"status\":\"failure\", \"message\":\"The profile update has failed.\"}");
				out.close();
			}
		}
	}
}
