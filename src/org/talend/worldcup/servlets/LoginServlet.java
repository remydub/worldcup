package org.talend.worldcup.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.talend.worldcup.data.UserData;

public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger log = Logger.getLogger(LoginServlet.class);
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);

		PrintWriter out = response.getWriter();
		if (session == null) {
			out.write("{\"status\":\"failure\", \"message\":\"The login has failed.\"}");
			out.close();			
		} else {
			out.write("{\"status\":\"success\", \"message\":\"The login has succeeded.\"}");
			out.close();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = null;
		
		BufferedReader in = request.getReader();
		String i;
		String message = null;
		String username = null;
		String password = null;
		while ((i = in.readLine()) != null) {
			message = i;
		}

		if (message != null) {
			JSONParser parser = new JSONParser();
			try {
				JSONObject objectToParse = (JSONObject) parser.parse(message);
				username = (String) objectToParse.get("username");
				password = (String) objectToParse.get("password");
				log.info("Logging attempt with <" + username + "> and <"
						+ password + ">");
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		boolean isAuthenticated = false;
		String exception = "";
		try {
			UserData data = new UserData();
			isAuthenticated = data.login(username, password);
		} catch (Exception e) {
			log.error(e.getMessage());
			exception = e.getMessage();
		}
		
		if(!isAuthenticated) {
			exception = "Authentication failed. Please be sure the password you have filled in is correct.";
		} else {
			session = request.getSession();
			session.setMaxInactiveInterval(3600);
		}
		
		if(session!=null) session.setAttribute("username", username);
		
		
		String jsonToSendBack = "{\"status\":\""+(isAuthenticated?"success":"failure")+"\", \"id\":\""+ (session!=null?session.getId():"") +"\", "+(isAuthenticated?"\"username\":\""+username+"\", \"message\":\"ok\"":"\"message\":\""+exception+"\"")+"}";
		PrintWriter out = response.getWriter();
		out.print(jsonToSendBack);
		out.close();
	}
}
