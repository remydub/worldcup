package org.talend.worldcup.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.talend.worldcup.data.DetailData;
import org.talend.worldcup.exception.NotStartedException;

public class DetailServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);

		PrintWriter out = response.getWriter();
		if (session == null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your session has expired. Please login.\"}");
			out.close();
		} else {
			DetailData data;
			try {
				data = new DetailData();
				String details = data.getDetails(Integer.parseInt(request.getParameter("gameid")));
				out.print(details);
			} catch (NotStartedException e) {
				out.print("{\"status\":\"failure\", \"code\":10, \"message\":\""+e.getMessage()+"\"}");
			} catch (Exception e) {
				out.print("{\"status\":\"failure\", \"code\":500, \"message\":\""+e.getMessage()+"\"}");
			} finally {
				out.close();
			}
		}
	}
}
