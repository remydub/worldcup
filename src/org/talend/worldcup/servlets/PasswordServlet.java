package org.talend.worldcup.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.talend.worldcup.data.UserData;
import org.talend.worldcup.data.common.Configuration;

public class PasswordServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(PasswordServlet.class);

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		BufferedReader in = request.getReader();
		String i;
		String username = null;
		String message = null;
		String password = null;
		String id = null;
		while ((i = in.readLine()) != null) {
			message = i;
		}

		if (message != null) {
			JSONParser parser = new JSONParser();
			try {
				JSONObject objectToParse = (JSONObject) parser.parse(message);
				username = (String) objectToParse.get("username");
				password = (String) objectToParse.get("password");
				id = (String) objectToParse.get("id");

				if (password != null) {
					log.info("Password update attempt with <" + username
							+ "> and <" + password + ">");
				} else {
					log.info("Password regeneration attempt with <" + username
							+ ">");
				}

			} catch (ParseException e) {
				log.info("Password regeneration attempt with <" + username
						+ ">");
			}
		}
		PrintWriter out = response.getWriter();
		if (password == null) {
			String to = username;
			String from = Configuration.INSTANCE.getAsString("email.address");
			String host = "smtp.gmail.com";
			Properties properties = System.getProperties();
			properties.setProperty("mail.smtp.host", host);
			properties.setProperty("mail.user", from);
			properties.setProperty("mail.password",
					Configuration.INSTANCE.getAsString("email.password"));
			Session session = Session.getDefaultInstance(properties);
			try {
				UserData data = new UserData();
				String asciiString = data.getRandomAsciiString(username);
				MimeMessage mimeMessage = new MimeMessage(session);
				mimeMessage.setRecipients(RecipientType.TO, to);
				mimeMessage.setSubject("Talend Worldcup Password Reset");
				mimeMessage.setContent(
						"In order to reset your password, please copy and paste this URL in your web browser: <h6>http://"+Configuration.INSTANCE.getAsString("server.hostname")+":8080/worldcup/#/passwordregeneration/"
								+ username + "/" + asciiString + "</h6>",
						"text/html; charset=utf-8");
				Transport t = session.getTransport("smtps");
				try {
					t.connect(host, Configuration.INSTANCE.getAsString("email.address"), Configuration.INSTANCE.getAsString("email.password"));
					t.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
					out.print("{\"status\":\"success\", \"message\":\"Password reset in progress. Please read your emails.\"}");
					out.close();
				} finally {
					t.close();
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				out.print("{\"status\":\"failure\", \"code\":500, \"message\":\""
						+ e.getMessage() + "\"}");
				out.close();
			}
		} else {
			UserData data;
			try {
				data = new UserData();
				data.updatePassword(username, password, id);
				out.print("{\"status\":\"success\", \"message\":\"Your password has been updated successfully.\"}");
				out.close();
			} catch (Exception e) {
				log.error(e.getMessage());
				out.print("{\"status\":\"failure\", \"code\":420, \"message\":\""
						+ e.getMessage() + "\"}");
			}
		}
	}
}
