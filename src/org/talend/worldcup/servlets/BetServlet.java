package org.talend.worldcup.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.talend.worldcup.data.BetData;
import org.talend.worldcup.data.ResultData;
import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.view.IBets;
import org.talend.worldcup.interfaces.view.IGames;
import org.talend.worldcup.utils.Utils;
import org.talend.worldcup.view.Bets;

public class BetServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(false);

		PrintWriter out = response.getWriter();
		if (session == null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your session has expired. Please login.\"}");
			out.close();
		} else {
			BetData betData;
			ResultData resultData;
			try {
				betData = new BetData();
				IBets bets = betData.getBets((String)session.getAttribute("username"));
				resultData = new ResultData();
				IGames gameList = resultData.getGames();
				
				JSONArray mergedJsonArray = Utils.mergeJSONArray(bets.getJSONArray(), gameList.getJSONArray());
				out.print(mergedJsonArray.toJSONString());
			} catch (Exception e) {
				out.print("{\"status\":\"failure\", \"code\":500, \"message\":\""+e.getMessage()+"\"}");
			} finally {
				out.close();
			}
		}
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();
		
		if (session == null) {
			out.print("{\"status\":\"failure\", \"code\":419, \"message\":\"Your bets have not been saved. Your session has expired. Please login.\"}");
			out.close();
		} else {
			BufferedReader in = request.getReader();
			String message="";
			String i;
			while ((i = in.readLine()) != null) {
				message = i;
			}
			
			try {
				List<IBet> betList = Bets.getBetsList(message, (String)session.getAttribute("username"));
				BetData data = new BetData();
				data.updateBets(betList);
				out.write("{\"status\":\"success\"");
			} catch (Exception e) {
				e.printStackTrace();
				out.write("{\"status\":\"failure\", \"exception\":\""+e.getMessage()+"\"");
			}
		}
		out.close();
	}
}
