package org.talend.worldcup.view;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.talend.worldcup.interfaces.business.IUser;
import org.talend.worldcup.interfaces.view.IRanking;

public class Ranking implements IRanking{
	private List<IUser> mRankingList;

	protected Ranking() {
		this.mRankingList = new ArrayList<IUser>();
	}
	
	protected Ranking(List<IUser> userList) {
		this.mRankingList = userList;
	}
	
	public List<IUser> getRankingList() { return mRankingList; }
	public void setRankingList(List<IUser> mRankingList) { this.mRankingList = mRankingList; }
	
	public void addUser(IUser user) {
		if(user!=null) {
			this.mRankingList.add(user);
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toString() {
		JSONArray jsonArray = new JSONArray();
		for(IUser user : this.mRankingList) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("name", user.getUserFirstname() + " " + user.getUserLastname().toUpperCase());
			jsonObject.put("score", user.getUserScore().getScore());
			jsonObject.put("unit", (user.getUserUnit().getUnitName()==null||"null".equals(user.getUserUnit().getUnitName()))?"":user.getUserUnit().getUnitName());
			jsonObject.put("company", user.getUserCompany().getCompanyName());
			jsonArray.add(jsonObject);
		}
		return jsonArray.toJSONString();
	}
}
