package org.talend.worldcup.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IUser;
import org.talend.worldcup.interfaces.view.IRanking;
import org.talend.worldcup.interfaces.view.IRankingFactory;

public class RankingFactory implements IRankingFactory {
	public IRanking getRanking() {
		return new Ranking();
	}
	
	public IRanking getRanking(List<IUser> userList) {
		return new Ranking(userList);
	}
}
