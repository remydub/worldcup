package org.talend.worldcup.view;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.view.IGroups;

public class Groups implements IGroups {
	private List<ITeam> mTeamList;

	protected Groups() {
		this.mTeamList = new ArrayList<ITeam>();
	}
	
	protected Groups(List<ITeam> teamList) {
		this.mTeamList = teamList;
	}

	protected Groups(List<ITeam> teamList, List<IGame> gameList) {
		this.mTeamList = teamList;
	}

	@SuppressWarnings("unchecked")
	public String toString() {
		JSONArray jsonArray = new JSONArray();
		boolean isFirst = true;
		JSONArray teamArray = null;
		String previousGroup = "";
		for (ITeam team : this.mTeamList) {
			String group = team.getTeamGroup().getGroupName();
			if(isFirst || !group.equals(previousGroup)) {
				JSONObject groupObject = new JSONObject();
				teamArray = new JSONArray();

				groupObject.put("name", group);
				groupObject.put("teams", teamArray);
				jsonArray.add(groupObject);
				isFirst = false;
			}
			JSONObject teamObject = new JSONObject();
			teamObject.put("id", team.getTeamId());
			teamObject.put("flag", team.getTeamFlag());
			teamObject.put("name", team.getTeamName());
			previousGroup = group;
			teamArray.add(teamObject);
		}
		return jsonArray.toJSONString();
	}
}
