package org.talend.worldcup.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.view.IGames;

public class Games implements IGames {
	private List<IGame> mGameList;

	protected Games() {
		this.mGameList = new ArrayList<IGame>();
	}

	protected Games(List<IGame> gameList) {
		this.mGameList = gameList;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getJSONArray() {
		JSONArray jsonArray = new JSONArray();
		for (IGame game : this.mGameList) {
			JSONObject gameObject = new JSONObject();
			int gameId = game.getGameId();
			boolean gamePlayed = game.isGamePlayed();
			Date gameDate = game.getGameDate();
			Date currentDate = new Date(new java.util.Date().getTime());
			
			gameObject.put("gameid", gameId);
			gameObject.put("gameplayed", (gamePlayed?1: 0));
			gameObject.put("gamestarted", (gameDate.before(currentDate)?1: 0));			
			gameObject.put("gamedate", gameDate.getTime());
			JSONArray teamArray = new JSONArray();
			gameObject.put("teams_game", teamArray);
			Map<ITeam, Integer> teamMap = game.getTeamOne();
			for (Entry<ITeam, Integer> e : teamMap.entrySet()) {
				JSONObject teamObject = new JSONObject();
				ITeam team = e.getKey();
				teamObject.put("teamid", team.getTeamId());
				teamObject.put("teamname", team.getTeamName());
				teamObject.put("teamflag", team.getTeamFlag());
				teamObject.put("score", e.getValue());
				teamArray.add(teamObject);
			}
			jsonArray.add(gameObject);
		}
		return jsonArray;
	}
	
	public String toString() {
		if(this.getJSONArray()!=null)
			return this.getJSONArray().toJSONString();
		else
			return "";
	}
}
