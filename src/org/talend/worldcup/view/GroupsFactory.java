package org.talend.worldcup.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.view.IGroups;
import org.talend.worldcup.interfaces.view.IGroupsFactory;

public class GroupsFactory implements IGroupsFactory {

	public IGroups getGroups() {
		return new Groups();
	}

	public IGroups getGroups(List<ITeam> teamList, List<IGame> gameList) {
		return new Groups(teamList, gameList);
	}
	
	public IGroups getGroups(List<ITeam> teamList) {
		return new Groups(teamList);
	}
}
