package org.talend.worldcup.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.view.IBets;
import org.talend.worldcup.interfaces.view.IBetsFactory;

public class BetsFactory implements IBetsFactory {
	public IBets getBets() {
		return new Bets();
	}

	public IBets getBets(List<IBet> betList) {
		return new Bets(betList);
	}
}
