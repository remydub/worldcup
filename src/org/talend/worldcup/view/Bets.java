package org.talend.worldcup.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.talend.worldcup.business.BetFactory;
import org.talend.worldcup.business.GameFactory;
import org.talend.worldcup.business.GroupFactory;
import org.talend.worldcup.business.TeamFactory;
import org.talend.worldcup.interfaces.business.IBet;
import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.business.IGroupFactory;
import org.talend.worldcup.interfaces.business.ITeam;
import org.talend.worldcup.interfaces.business.ITeamFactory;
import org.talend.worldcup.interfaces.view.IBets;

public class Bets implements IBets {
	private List<IBet> mBetList;

	protected Bets() {
		this.mBetList = new ArrayList<IBet>();
	}

	protected Bets(List<IBet> betList) {
		this.mBetList = betList;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getJSONArray() {
		JSONArray jsonArray = new JSONArray();
		for (IBet bet : this.mBetList) {
			IGame game = bet.getGame();
			JSONObject gameObject = new JSONObject();
			int gameId = game.getGameId();
			boolean gamePlayed = game.isGamePlayed();
			Date gameDate = game.getGameDate();
			Date currentDate = new Date(new java.util.Date().getTime());
						
			gameObject.put("gameid", gameId);
			gameObject.put("gameplayed", (gamePlayed?1: 0));
			gameObject.put("gamestarted", (gameDate.before(currentDate)?1: 0));
			gameObject.put("gamedate", gameDate.getTime());
			JSONArray teamArray = new JSONArray();
			gameObject.put("teams_bet", teamArray);
			Map<ITeam, Integer> teamMap = game.getTeamOne();
			for (Entry<ITeam, Integer> e : teamMap.entrySet()) {
				JSONObject teamObject = new JSONObject();
				ITeam team = e.getKey();
				teamObject.put("teamid", team.getTeamId());
				teamObject.put("teamgroup", new Character(team.getTeamGroup().getGroupId()).toString());
				teamObject.put("teamname", team.getTeamName());
				teamObject.put("teamflag", team.getTeamFlag());
				teamObject.put("score", e.getValue());
				teamArray.add(teamObject);
			}
			jsonArray.add(gameObject);
		}
		return jsonArray;
	}
	
	public String toString() {
		if(this.getJSONArray()!=null)
			return this.getJSONArray().toJSONString();
		else
			return "";
	}

	public static List<IBet> getBetsList(String message, String username) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONArray array = (JSONArray) parser.parse(message);
		IGame game = null;
		List<IBet> betList = new ArrayList<IBet>();
		for (int i = 0; i<array.size(); i++) {
			JSONObject object = (JSONObject) array.get(i);
			Map<ITeam, Integer> map = new LinkedHashMap<ITeam, Integer>();
			JSONArray teams = (JSONArray)object.get("teams_bet");
			ITeamFactory teamFact = new TeamFactory();
			IGroupFactory groupFact = new GroupFactory();
			for (int j=0; j<teams.size(); j++) {
				JSONObject team = (JSONObject)teams.get(j);
				Object o = team.get("score");
				if(o==null || o instanceof java.lang.String) {
					map.put(teamFact.getTeam(((Long)team.get("teamid")).intValue(), (String)team.get("teamflag"), (String)team.get("teamname"), groupFact.getGroup(((String)team.get("teamgroup")).charAt(0), "")), (team.get("score")!=null && !"".equals(team.get("score")))?Integer.parseInt((String)team.get("score")):null);
				} else {
					map.put(teamFact.getTeam(((Long)team.get("teamid")).intValue(), (String)team.get("teamflag"), (String)team.get("teamname"), groupFact.getGroup(((String)team.get("teamgroup")).charAt(0), "")), (team.get("score")!=null && !"".equals(team.get("score")))?((Long)team.get("score")).intValue():null);
				}
			}
			game = new GameFactory().getGame(((Long)object.get("gameid")).intValue(), (Long)object.get("gameplayed")==1, new Date((Long)object.get("gamedate")), map);
			IBet bet = new BetFactory().getBet(game, username);
			betList.add(bet);
		}
		return betList;
	}
}
