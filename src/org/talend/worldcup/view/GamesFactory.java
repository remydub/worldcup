package org.talend.worldcup.view;

import java.util.List;

import org.talend.worldcup.interfaces.business.IGame;
import org.talend.worldcup.interfaces.view.IGames;
import org.talend.worldcup.interfaces.view.IGamesFactory;

public class GamesFactory implements IGamesFactory {
	public IGames getGames() {
		return new Games();
	}

	public IGames getGames(List<IGame> gameList) {
		return new Games(gameList);
	}
}
