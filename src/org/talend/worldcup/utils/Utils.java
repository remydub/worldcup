package org.talend.worldcup.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Utils {
	@SuppressWarnings("unchecked")
	public static JSONArray mergeJSONArray(JSONArray array1, JSONArray array2) {
		for (int i = 0; i<array1.size(); i++) {
			JSONObject object = (JSONObject) array1.get(i);
			object.put("teams_game", (JSONArray) ((JSONObject) array2.get(i)).get("teams_game"));
		}
		return array1;
	}
}
