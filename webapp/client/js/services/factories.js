myWorldcupApp.factory('MenuFactory', function($http) {
	var pages = [ {
		name : 'home',
		url : ''
	}, {
		name : 'profile',
		url : 'profile'
	}, {
		name : 'rules',
		url : 'rules'
	}, {
		name : 'groups',
		url : 'groups'
	}, {
		name : 'bets',
		url : 'bets'
	}, {
		name : 'ranking',
		url : 'rankings'
	} ];

	var factory = {};
	factory.getMenuPages = function() {
		return pages;
	};

	factory.checkConnection = function() {
		var get = $http.get('login?preventCache='+Math.random());
		return get;
	};
	
	return factory;
});

myWorldcupApp.factory('RankingFactory', function($http) {

	var departments = [ {
		departmentName : ''
	}, {
		departmentName : 'R&D'
	}, {
		departmentName : 'Marketing'
	} ];

	var companies = [ {
		companyName : ''
	}, {
		companyName : 'Talend'
	}, {
		companyName : 'Synotis'
	} ];

	var factory = {};
	factory.getRanking = function() {
		var get = $http.get('ranking?preventCache='+Math.random());
		return get;
	};

	factory.getCompanies = function() {
		return companies;
	};

	factory.getDepartments = function() {
		return departments;
	};

	return factory;
});

myWorldcupApp.factory('GroupsFactory', function($http) {
	var factory = {};
	factory.getGroups = function() {
		var get = $http.get('group?preventCache='+Math.random());
		return get;
	};
	return factory;
});

myWorldcupApp.factory('ResultFactory', function($http) {
	var factory = {};
	factory.getResults = function() {
		var get = $http.get('result?preventCache='+Math.random());
		return get;
	};
	return factory;
});

myWorldcupApp.factory('BetFactory', function($http) {
	var factory = {};
	factory.getBets = function() {
		var get = $http.get('bet?preventCache='+Math.random());
		return get;
	};

	factory.setBets = function(bets) {
		var post = $http.post('bet?preventCache='+Math.random(), bets);
		return post;
	};
	return factory;
});

myWorldcupApp.factory('DetailFactory', function($http) {
	var factory = {};
	factory.getDetailedResults = function(gameid) {

		var get = $http({
			method : 'GET',
			url : 'detail?preventCache='+Math.random()+'&gameid=' + gameid,
		});
		return get;
	};
	return factory;
});

myWorldcupApp.factory('ProfileFactory', function($http, md5) {
	
	var factory = {};
	
	factory.updateprofile = function(firstname, lastname) {
		var message = "{\"firstname\":\""+firstname.replace(new RegExp("\\\\", 'g'), "\\\\\\\\").replace(new RegExp("\"", 'g'), "\\\"")+"\", \"lastname\":\""+lastname.replace(new RegExp("\\\\", 'g'), "\\\\\\\\").replace(new RegExp("\"", 'g'), "\\\"")+"\"}";
		var post = $http.post('profile?preventCache='+Math.random(), message);
		return post;
	};

	factory.getprofile = function() {
		var get = $http.get('profile?preventCache='+Math.random());
		return get;
	};
	
	return factory;
});

myWorldcupApp.factory('LoginFactory', function($http, md5) {
	var companies = [ {
		companyName : 'Talend',
		companyPassword : '12345'
	}, {
		companyName : 'Synotis',
		companyPassword : '123456'
	} ];

	var departments = [ {
		departmentName : 'R&D'
	}, {
		departmentName : 'Marketing'
	} ];

	var factory = {};

	factory.getLogin = function(username, password) {
		var message = "{\"username\":\"" + username + "\", \"password\":\""
				+ md5.createHash(password) + "\"}";
		var post = $http.post('login?preventCache='+Math.random(), message);
		return post;
	};

	factory.sendemail = function(email) {
		var message = "{\"username\":\"" + email + "\"}";
		var post = $http.post('password?preventCache='+Math.random(), message);
		return post;
	};

	factory.updatepassword = function(email, password, id) {
		var message = "{\"username\":\"" + email + "\", \"password\":\""
				+ md5.createHash(password) + "\", \"id\":\"" + id + "\"}";
		var post = $http.post('password?preventCache='+Math.random(), message);
		return post;
	};
	
	factory.getRegistration = function(username, password, firstname, lastname,
			companyname, companypassword, departmentname) {
		var message = "{\"username\":\"" + username + "\", \"password\":\""
				+ md5.createHash(password) + "\", \"firstname\":\"" + firstname.replace(new RegExp("\\\\", 'g'), "\\\\\\\\").replace(new RegExp("\"", 'g'), "\\\"")
				+ "\", \"lastname\":\"" + lastname.replace(new RegExp("\\\\", 'g'), "\\\\\\\\").replace(new RegExp("\"", 'g'), "\\\"") + "\", \"companyname\":\""
				+ companyname + "\", \"companypassword\":\"" + companypassword
				+ "\", \"departmentname\":\"" + departmentname + "\"}";
		var post = $http.post('register?preventCache='+Math.random(), message);
		return post;
	};

	factory.getCompanies = function() {
		return companies;
	};

	factory.getDepartments = function() {
		return departments;
	};

	return factory;
});
