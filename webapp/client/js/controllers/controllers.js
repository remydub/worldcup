var myWorldcupApp = angular.module('myWorldcupApp', [ 'ngRoute', 'ngMd5',
		'ui.bootstrap' ]);

function MenuController($scope, $route, $http, $location, MenuFactory) {

	MenuFactory.checkConnection().success(function(data) {
		status = data.status;
		if (status == 'failure') {
			if ($route.current.requireLogin == true) {
				$location.path('/login');
			}
		}
	});

	// pages is a JSON array containing the list of choices in the menu.
	$scope.pages = MenuFactory.getMenuPages();

	// init the index to -1.
	$scope.selectedIndex = -1;
	$scope.highlightedItem = 0;

	$scope.onMouseover = function(i) {
		$scope.highlightedItem = i;
	};

	$scope.$watch('highlightedItem', function(n, o) {
		$("#" + o).removeClass("selected-header" + o);
		$("#" + n).addClass("selected-header" + n);
	});

	// function which allows to define the index of the menu we have clicked on
	// and the page URL.
	$scope.menuClicked = function($index) {
		$scope.selectedIndex = $index;
		$scope.url = $scope.pages[$index].url;
	};
};

// this method retrieves the ranking from the server.
function RankingController($scope, $http, $location, RankingFactory) {
	$scope.ranks = [];
	RankingFactory.getRanking().success(function(data) {
		if (data.status == 'failure') {
			if (data.code == '419') {
				$location.path('/login');
			}
		}
		$scope.ranks = data;
	});

	$scope.companies = RankingFactory.getCompanies();
	$scope.departments = RankingFactory.getDepartments();
};

// this method retrieves the results from the server.
function ResultController($scope, $http, $location, ResultFactory) {
	$scope.results = [];

	ResultFactory.getResults().success(function(data) {
		if (data.status == 'failure') {
			if (data.code == '419') {
				$location.path('/login');
			}
		}
		$scope.results = data;
	});
};

// this method retrieves the bets from the server.
function BetController($scope, $http, $location, $dialog, BetFactory) {
	$scope.bets = [];

	BetFactory.getBets().success(function(data) {
		if (data.status == 'failure') {
			if (data.code == '419') {
				$location.path('/login');
			}
		}
		$scope.bets = data;
	});

	$scope.save = function() {
		BetFactory.setBets($scope.bets).success(
				function(data) {
					if (data.status == 'failure') {
						if (data.code == '419') {
							var msgbox = $dialog.messageBox('Bets not saved',
									data.message, [ {
										label : 'Ok',
										result : 'ok'
									} ]);
							msgbox.open().then(function(result) {
								if (result == 'ok') {
									$location.path('/login');
								}
								;
							});
						}
					} else {
						var msgbox = $dialog.messageBox('Bet confirmation',
								'Your bets have been saved successfully.', [ {
									label : 'Ok',
									result : 'ok'
								} ]);
						msgbox.open();
					}
				});
	};
};

// this method retrieves the results from the server.
function DetailController($scope, $http, $location, $routeParams, $dialog,
		DetailFactory) {
	$scope.bets = [];
	$scope.gameid = $routeParams.gameid;

	DetailFactory.getDetailedResults($routeParams.gameid).success(
			function(data) {
				if (data.status == 'failure') {
					if (data.code == '419') {
						$location.path('/login');
					}
					if (data.code == '10') {
						var msgbox = $dialog.messageBox('Not started game',
								data.message, [ {
									label : 'Ok',
									result : 'ok'
								} ]);
						msgbox.open().then(function(result) {
							if (result == 'ok') {
								$location.path('/results');
							}
							;
						});
					}
				} else {
					$scope.bets = data;
				}
			});
};

// this method retrieves the groups from the server.
function GroupsController($scope, $http, $location, $dialog, GroupsFactory) {
	$scope.groups = "[]";
	GroupsFactory.getGroups().success(
			function(data) {
				if (data.status == 'failure') {
					if (data.code == '419') {
						$location.path('/login');
					}
					if (data.code == '500') {
						var msgbox500 = $dialog.messageBox('Server error',
								data.message, [ {
									label : 'Ok',
									result : 'ok'
								} ]);
						msgbox500.open();
					}
				}
				$scope.groups = data;
			});
}

function ProfileController($scope, $http, $location, $dialog, ProfileFactory) {

	$scope.profile = "";

	$scope.updateprofile = function(firstname, lastname) {
		ProfileFactory.updateprofile(firstname, lastname).success(
				function(data) {
					var msgbox = $dialog.messageBox('Profile updated',
							data.message, [ {
								label : 'Ok',
								result : 'ok'
							} ]);
					msgbox.open();
				});
	};

	$scope.profile = ProfileFactory.getprofile().success(function(data) {
		if (data.status == 'failure') {
			if (data.code == '419') {
				$location.path('/login');
			}
		} else {
			$scope.profile = data;
		}
	});
}

function LoginController($rootScope, $scope, $http, $location, $dialog,
		$routeParams, LoginFactory, md5) {
	$scope.isConnected = "";
	$scope.username = "";
	$scope.password = "";
	$scope.firstname = "";
	$scope.lastname = "";
	$scope.companyname = "";
	$scope.companypassword = "";
	$scope.departmentname = "";

	$scope.login = function(username, password) {
		if (username == '' || username == 'undefined') {
			var msgbox = $dialog
					.messageBox(
							'Login failed',
							'Please re-enter your username and password even if they have already been settled as they have not been taken into account by your browser.',
							[ {
								label : 'Ok',
								result : 'ok'
							} ]);
			msgbox.open();
		} else {
			LoginFactory.getLogin(username, password).success(
					function(data) {
						$scope.isConnected = data.status == 'success';
						$rootScope.loggedIn = data.status == 'success' ? true
								: false;
						if ($scope.isConnected) {
							$location.path('/');
						} else {
							var msgbox = $dialog.messageBox('Login failed',
									data.message, [ {
										label : 'Ok',
										result : 'ok'
									} ]);
							msgbox.open();
						}
					});
		}
	};

	$scope.register = function(username, password, firstname, lastname,
			companyname, companypassword, department) {
		LoginFactory.getRegistration(username, password, firstname, lastname,
				companyname, companypassword, department).success(
				function(data) {
					$scope.isRegistered = data.status == 'success';

					if ($scope.isRegistered) {
						var msgbox = $dialog.messageBox(
								'Registration confirmation', data.message, [ {
									label : 'Ok',
									result : 'ok'
								} ]);
						msgbox.open().then(function(result) {
							if (result == 'ok') {
								$location.path('/');
							}
							;
						});

					} else {
						var msgbox = $dialog.messageBox('Registration failed',
								data.message, [ {
									label : 'Ok',
									result : 'ok'
								} ]);
						msgbox.open();
					}
				});
	};

	$scope.sendemail = function(email) {
		LoginFactory.sendemail(email).success(function(data) {
			var msgbox = $dialog.messageBox('Email sent', data.message, [ {
				label : 'Ok',
				result : 'ok'
			} ]);
			msgbox.open().then(function(result) {
				if (result == 'ok') {
					$location.path('/');
				}
				;
			});
		});
	};

	$scope.updatepassword = function(password) {
		LoginFactory.updatepassword($routeParams.email, password,
				$routeParams.id).success(function(data) {
			var msgbox = $dialog.messageBox('Email sent', data.message, [ {
				label : 'Ok',
				result : 'ok'
			} ]);
			msgbox.open().then(function(result) {
				if (result == 'ok') {
					$location.path('/');
				}
				;
			});
		});
	};

	$scope.companies = LoginFactory.getCompanies();
	$scope.departments = LoginFactory.getDepartments();

}

// we define the routes.
myWorldcupApp.config(function($routeProvider) {
	$routeProvider.when('/', {
		controller : 'Menu',
		templateUrl : 'client/partials/home.html',
		requireLogin : true
	});
	$routeProvider.when('/rules', {
		controller : 'Menu',
		templateUrl : 'client/partials/rules.html',
		requireLogin : true
	});
	$routeProvider.when('/groups', {
		controller : 'Group',
		templateUrl : 'client/partials/groups.html',
		requireLogin : true
	});
	$routeProvider.when('/bets', {
		controller : 'Bet',
		templateUrl : 'client/partials/bets.html',
		requireLogin : true
	});
	$routeProvider.when('/results', {
		controller : 'Result',
		templateUrl : 'client/partials/results.html',
		requireLogin : true
	});
	$routeProvider.when('/results/details/:gameid', {
		controller : 'Detail',
		templateUrl : 'client/partials/details.html',
		requireLogin : true
	});
	$routeProvider.when('/rankings', {
		controller : 'Ranking',
		templateUrl : 'client/partials/ranking.html',
		requireLogin : true
	});
	$routeProvider.when('/login', {
		controller : 'Login',
		templateUrl : 'client/partials/login.html',
		requireLogin : false
	});
	$routeProvider.when('/profile', {
		controller : 'Profile',
		templateUrl : 'client/partials/profile.html',
		requireLogin : true
	});
	$routeProvider.when('/register', {
		controller : 'Login',
		templateUrl : 'client/partials/register.html',
		requireLogin : false
	});
	$routeProvider.when('/passwordlost', {
		controller : 'Login',
		templateUrl : 'client/partials/passwordlost.html',
		requireLogin : false
	});
	$routeProvider.when('/passwordregeneration/:email/:id', {
		controller : 'Login',
		templateUrl : 'client/partials/passwordregeneration.html',
		requireLogin : false
	});
});

myWorldcupApp.controller('Menu', MenuController);
myWorldcupApp.controller('Ranking', RankingController);
myWorldcupApp.controller('Group', GroupsController);
myWorldcupApp.controller('Result', ResultController);
myWorldcupApp.controller('Login', LoginController);
myWorldcupApp.controller('Profile', ProfileController);
myWorldcupApp.controller('Detail', DetailController);
myWorldcupApp.controller('Bet', BetController);
